/* 
 * Pawel Kubik
 * Main
 */

#include "Tester.h"

using namespace std;

int main()
{
	Tester t;
	
	while(!t.quitFlag()) {
		t.passControl();
	}
	
	return 0;
}

