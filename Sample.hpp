/* 
 * Pawel Kubik
 * Przyklad (test)
 */

#include <vector>
#include <initializer_list>

using namespace std;

#ifndef SAMPLE_HPP
#define	SAMPLE_HPP

template <class T>
class Sample {
private:
	vector<T>* vec;
	int* count;
	
	void destruct() {
		--(*count);
		if(*count == 0) {
			delete vec;
			delete count;
		}
	}
public:
	Sample() : vec(new vector<T>()), count(new int(1)) {}
	
	Sample(const vector<T>& vecr) : vec(new vector<T>(vecr)), count(new int(1)) {}
	
	Sample(initializer_list<T> il) : vec(new vector<T>(il)), count(new int(1)) {}
	
	Sample(const Sample& cp) : vec(cp.vec), count(cp.count) {
		++(*count);
	}
	
	Sample& operator= (const Sample& as) {
		destruct();
		vec = as.vec;
		count = as.count;
		++(*count);
	}
	
	unsigned int size() const { return vec->size(); }
	
	void push_back(const T& el) {
		vec->push_back(el);
	}
	
	void emplace_back(initializer_list<T> il) {
		vec->emplace_back(il);
	}
	
	void pop_back() {
		vec->pop_back();
	}
	
	bool empty() const {
		return vec->empty();
	}
	
	const T& at(int index) const { return vec->at(index); }
	
	const T& operator[] (int index) const { return vec->at(index); }
	
	void clear() {
		vec->clear();
	}
	
	virtual ~Sample() {
		destruct();
	}
};

#endif /*SAMPLE_HPP*/