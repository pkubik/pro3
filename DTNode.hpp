 /* 
 * Pawel Kubik
 * Wierzcholek decyzyjny
 */
 
#include <vector>
#include "Sample.hpp"

using namespace std;

#ifndef DTNODE_HPP
#define DTNODE_HPP

template <class T>
class DTNode {
public:
	virtual const T& getResult(const Sample<T>& sam) const = 0;
	
	virtual ~DTNode<T>() {}
};

#endif	/* DTNODE_HPP */

