/* 
 * Pawel Kubik
 * Procesor komend tekstowych
 */

#include "CommandProcessor.h"
#include <iostream>
#include <iomanip>

Command::Command(const string& des, function<void()> f) :
	desc(des),
	fn(f) {}

void Command::operator ()() const {
	fn();
}

CommandProcessor::CommandProcessor(function<void()> fun) : def(fun) {}

void CommandProcessor::insert(const string& cmd, const string& desc, function<void()> fun) {
	functions.insert( pair<string,Command>(cmd, {desc, fun}) );
}

void CommandProcessor::call(const string& cmd) {
	map<string,Command>::const_iterator it = functions.find(cmd);
	if(it!=functions.end()) 
		it->second();
	else
		def();
}

void CommandProcessor::description() const {
	for(map<string, Command>::const_iterator it=functions.begin();it!=functions.end();++it) {
		cout<<setw(26)<<left<<it->first<<" : "<<it->second.desc<<"\n";
	}
}
