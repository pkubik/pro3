/* 
 * Pawel Kubik
 * Implementacja konkretnego drzewa
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <set>
#include "Tester.h"

const char NON_ALPHANUM_VALID_CHARACTERS[] = {'?','-','!'};
const set<char> NAVC(NON_ALPHANUM_VALID_CHARACTERS, NON_ALPHANUM_VALID_CHARACTERS+sizeof(NON_ALPHANUM_VALID_CHARACTERS));

#define METHOD(x, ...) bind(&Tester::x, ref(*this), ##__VA_ARGS__)
Tester::Tester() : quit_flag(false), train_validation_flag(NO_CHECK), test_validation_flag(NO_CHECK), cp(unrecognisedCommand) {
	cp.insert("load train", "load training samples from a file", METHOD(loadSamples, ref(train_samples), ref(train_validation_flag)));
	cp.insert("load test", "load testing samples from a file", METHOD(loadSamples, ref(test_samples), ref(test_validation_flag)));
	cp.insert("validate train", "check whether loaded training samples are valid", METHOD(printTrainValidation));
	cp.insert("validate test", "check whether loaded testing samples are valid", METHOD(printTestValidation));
	cp.insert("add train", "input training sample from standard input", METHOD(addSample, ref(train_samples), ref(train_validation_flag)));
	cp.insert("add test", "input testing sample from standard input", METHOD(addSample, ref(test_samples), ref(test_validation_flag)));
	cp.insert("quit", "quit", METHOD(quit));
	cp.insert("test one", "test sample from standard input", METHOD(testSample));
	cp.insert("init", "initialise tree with loaded samples", METHOD(initTree));
	cp.insert("test all", "test all loaded samples", METHOD(groupTest));
	cp.insert("clear tree", "erase tree content", METHOD(clearTree));
	cp.insert("clear train", "erase training samples", METHOD(clearTrain));
	cp.insert("clear test", "erase testing samples", METHOD(clearTest));
	cp.insert("help", "print help", METHOD(printHelp));
}
#undef METHOD

void Tester::loadSamples(vector< Sample<char> >& samples, ValidationState& v_flag) {
	string text;
	cout<<"Enter the name of file: ";
	getline(cin, text);
	ifstream fs;
	fs.open(text.c_str());
	if(!fs) {
		cout<<"Failed to read file \""<<text<<"\".\n";
	}
	else {
		while(fs) {
			samples.emplace_back();
			fs>>samples.back();
			
			if(samples.back().empty())
				samples.pop_back();
		}
		v_flag = NO_CHECK;
	}
}

void Tester::initTree() {
	if(train_samples.empty()) {
		cout<<"No samples loaded!\n";
	}
	else if(!validateTrainSamples()) {
		cout<<"Samples are incorrect!\n";
	}
	else {
		cout<<"Enter the number of target attribute: ";
		unsigned int number;
		string line;
		getline(cin,line);
		stringstream ln(line);
		ln>>ws>>number>>ws;
		if(ln && ln.eof()) {
			if(number<train_samples.back().size()) {
				try {
					dt.initTrain(train_samples, number);
				}
				catch(const exception& ex) {
					cout<<"Failed to initialise tree.\n";
					throw;
				}
			}
			else {
				cout<<"Attribute number is out of range.\n";
			}
		}
		else {
			cout<<"Incorrect input.\n";
		}
	}
}

bool Tester::validateTestSample(const Sample<char>& sam) const {
	return (dt.isInit() && sam.size() == dt.getSampleSize());
}

bool Tester::validateTrainSamples() const {
	if(!train_samples.empty())
		return validateSamples(train_samples, train_validation_flag, train_samples.back().size());
	else
		return false;
}

bool Tester::validateTestSamples() const {
	if(!test_samples.empty())
		if(dt.isInit())
			return validateSamples(test_samples, test_validation_flag, dt.getSampleSize());
		else
			return true;
	else
		return false;
}

bool Tester::validateSamples(const vector< Sample<char> >& samples, ValidationState& v_flag, unsigned int size) const {
	if(v_flag == NO_CHECK) {
		v_flag = YES;
		if (size == 0)
			v_flag = NO;
		else {
			for (vector< Sample<char> >::const_iterator it = samples.begin(); it != samples.end(); ++it) {
				if (size != it->size()) {
					v_flag = NO;
					break;
				}
			}
		}
	}
	
	return v_flag;
}

void Tester::printTrainValidation() const {
	if(validateTrainSamples()) {
		cout<<"Samples are valid.\n";
	}
	else {
		cout<<"Samples are invalid.\n";
	}
}

void Tester::printTestValidation() const {
	if(validateTestSamples()) {
		cout<<"Samples are valid.\n";
	}
	else {
		cout<<"Samples are invalid.\n";
	}
}

void Tester::testSample() const {
	if(dt.isInit()) {
		cout<<"Enter the sample: ";
		Sample<char> sam;
		cin>>sam;
		if(validateTestSample(sam)) {
			if(dt.test(sam)) {
				cout<<"Test result is correct.\n";
			}
			else {
				cout<<"Test result is incorrect.\n";
			}
		}
		else {
			cout<<"Incorrect sample.\n";
		}
	}
	else {
		cout<<"Initialise the tree first!\n";
	}
}

void Tester::groupTest() const {
	if(dt.isInit()) {
		if(validateTestSamples())
			cout<<dt.groupTest(test_samples)*100<<"% of tests are successful.\n";
		else
			cout<<"Invalid test samples.\n";
	}
	else
		cout<<"Initialise the tree first!\n";
}

void Tester::passControl() {
	try {
		cout<<"#> ";
		string text;
		getline(cin, text);
		cp.call(text);
	}
	catch(const bad_alloc& ba) {
		cout<<"Inable to allocate required memory.\n";
		cerr<<ba.what();
	}
	catch(const exception& ex) {
		cout<<"Unspecified problem occurred. Operation failed.\n";
		cerr<<ex.what()<<"\n";
	}
}

void Tester::addSample(vector< Sample<char> >& samples, ValidationState& v_flag) {
	cout<<"Enter the sample: ";
	samples.emplace_back();
	
	cin>>samples.back();

	if(samples.back().empty()) {
		samples.pop_back();
		cout<<"Empty sample!\n";
	}
	else {
		v_flag = NO_CHECK;
	}
}

void Tester::clearTrain() {
	train_samples.clear();
}

void Tester::clearTest() {
	test_samples.clear();
}

void Tester::clearTree() {
	dt.clear();
}

void Tester::printHelp() const {
	cout<<"List of available commands:\n";
	cp.description();
}

void Tester::unrecognisedCommand() {
	cout<<"Unrecognised command. Type \"help\" for a list of available commands.\n";
}

istream& operator >>(istream& is, Sample<char>& sam) {
	char ch;
	do {
		ch = is.get();
		if(isalnum(ch) || NAVC.find(ch) != NAVC.end()) {
			sam.push_back(ch);
		}
	} while(!is.eof() && ch != '\n');
	return is;
}
