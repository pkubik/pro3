CC=g++
CFLAGS=-ggdb3 -Wall -std=c++11
LFLAGS=-g
EXECUTABLE=pro3
SOURCES=$(wildcard *.cpp)
OBJS=$(SOURCES:.cpp=.o)

all: .Make.dep $(SOURCES) $(EXECUTABLE)

-include .Make.dep

$(EXECUTABLE): $(OBJS)
	@echo "Budowanie $(EXECUTABLE)"
	$(CC) $(OBJS) $(LFLAGS) -o $(EXECUTABLE)

.cpp.o:
	@echo "Budowanie $@"
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	rm -f ./.Make.dep
	
.Make.dep: $(SRCS)
	rm -f ./.Make.dep
	$(CC) $(CFLAGS) $(SOURCES) -MM $^>>./.Make.dep
	