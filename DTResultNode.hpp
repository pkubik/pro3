/* 
 * Pawel Kubik
 * Wierzcholek wynikowy (lisc)
 */

#include "DTNode.hpp"

using namespace std;

#ifndef DTRESULTNODE_HPP
#define DTRESULTNODE_HPP

template <class T>
class DTResultNode : public DTNode<T> {
private:
	T res;
public:
	DTResultNode<T>(T r) { res = r; }
	
	virtual const T& getResult(const Sample<T>& sam) const {
		return res;
	}
};

#endif	/* DTRESULTNODE_HPP */