/* 
 * Pawel Kubik
 * Procesor komend tekstowych
 */

#include <map>
#include <string>
#include <functional>
#include <initializer_list>

using namespace std;

#ifndef COMMANDPROCESSOR_H
#define	COMMANDPROCESSOR_H

struct Command {
	const string desc;
	function<void()> fn;
	Command(const string& des, function<void()> f);
	void operator ()() const;
};

class CommandProcessor {
private:
	map<string, Command> functions;
	function<void()> def;
public:
	CommandProcessor(function<void()> fun);
	void insert(const string& cmd, const string& desc, function<void()> fun);
	void call(const string& cmd);
	void description() const;
	
	template <class T>
	void insert(const string& cmd, const string& desc, T& ob, void (T::* fun)()) {
		functions.insert( pair<string,Command>(cmd, Command(desc, bind(fun, ref(ob)) ) ));
	}

	template <class T>
	void insert(const string& cmd, const string& desc, T& ob, void (T::* fun)() const) {
		functions.insert( pair<string,Command>(cmd, Command(desc, bind(fun, ref(ob)) ) ));
	}
};

#endif	/* COMMANDPROCESSOR_H */

