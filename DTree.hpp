/* 
 * Pawel Kubik
 * Drzewo decyzyjne
 */

#include <vector>
#include "DTNode.hpp"
#include "Sample.hpp"
#include "DTTestNode.hpp"
#include "Error.h"

#ifndef DTREE_HPP
#define DTREE_HPP

struct TreeNotInitialised : virtual public logic_error {
	TreeNotInitialised() : logic_error("tree not initialised") {}
};

struct TreeInitialised : virtual public logic_error {
	TreeInitialised() : logic_error("tree already initialised") {}
};

struct InvalidTreeTarget : virtual public logic_error {
	InvalidTreeTarget() : logic_error("target out of samples range") {}
};

template <class T, class I>
class DTree {
private:
	I root;
	unsigned int s_size;
	unsigned int target;
	bool is_init;
public:
	DTree() : is_init(false) {}
	
	unsigned int getSampleSize() const { return s_size; }
	
	bool isInit() const { return is_init; }	

	const T& evaluate(const Sample<T>& sam) const {
		if(!is_init)
			throw TreeNotInitialised();
		else if(sam.size() != s_size)
			throw InvalidSampleSize(s_size);
		return root.getResult(sam);
	}	

	bool test(const Sample<T>& sam) const {
		return evaluate(sam) == sam[target];
	}
	
	double groupTest(const vector< Sample<T> >& samples) const {
		if(samples.empty())
			throw EmptyArgument("samples in DTree::groupTest");
		
		unsigned int i = 0;
		unsigned int l = 0;
		for(typename vector< Sample<T> >::const_iterator it=samples.begin();it!=samples.end();++it) {
			if(test(samples[i]))
				++l;
			i++;
		}
		
		return double(l)/i;
	}
	
	void initTrain(const vector< Sample<T> >& samples, unsigned int c) {
		if(is_init)
			throw TreeInitialised();
		else if(samples.empty())
			throw EmptyArgument("samples in DTree::initTrain");
		else {
			s_size = samples.front().size();
			if(s_size <= c)
				throw InvalidTreeTarget();
			else
				root.initialTrain(samples, c);
		}
		target = c;
		is_init = true;
	}
	
	void clear() {
		root.clearBranch();
		is_init = false;
	}
};

#endif	/* DTREE_HPP */

