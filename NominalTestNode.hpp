/* 
 * Pawel Kubik
 * Wierzcholek testujacy atrybuty o wartosciach nominalnych
 */

#include "DTTestNode.hpp"
#include <map>
#include <cmath> 

using namespace std;

#ifndef NOMINALTESTNODE_HPP
#define	NOMINALTESTNODE_HPP

template <class T, T U> //T musi miec domyslny konstruktor i operator <
class NominalTestNode : public DTTestNode<T> {
private:
	struct Quantity {
		int direct;					//ilosc wystapien danego atrybutu
		map<T,int> intersection;	//ilosc wystopien danego atrybutu kiedy pojecie = c(t)

		Quantity() { direct = 0; }

		void count(T c) {
			++direct;
			++intersection[c];
		}
	};
	
	int attr_i;
	map<T,int> attr_v; 
	
	void computeEntrophy(const vector< Sample<T> >& samples, unsigned int c, vector< map<T, Quantity> >& q) {
		const unsigned int s_size = samples.front().size();
		
		q.resize(s_size);
		
		for(typename vector< Sample<T> >::const_iterator s_it=samples.begin();s_it!=samples.end();++s_it) {
			if(s_it->size() != s_size) {
				throw InvalidSampleSize(s_size);
			}
			for(unsigned int i=0;i<s_it->size();++i) {
				q[i][s_it->at(i)].count(s_it->at(c));
			}
		}

		double max_ent = 0;
		double inf = 0;

		for(typename map<T,Quantity>::const_iterator c_it=q[c].begin();c_it!=q[c].end();++c_it) {
			inf -= (double(c_it->second.direct)/samples.size()) * log2(double(c_it->second.direct)/samples.size());
		}

		for(unsigned int i=0;i<s_size;++i) {
			if(i != c) {
				double ent_t = 0;
				double iv = 0;
				for(typename map<T,Quantity>::const_iterator m_it=q[i].begin();m_it!=q[i].end();++m_it) {
					double ent_tr = 0;
					for(typename map<T,int>::const_iterator c_it=m_it->second.intersection.begin();c_it!=m_it->second.intersection.end();++c_it) {					
						ent_tr -= (double(c_it->second)/m_it->second.direct) * log2(double(c_it->second)/m_it->second.direct);
					}
					ent_t += (double(m_it->second.direct)/samples.size()) * ent_tr;
					iv -= (double(m_it->second.direct)/samples.size()) * log2(double(m_it->second.direct)/samples.size());
				}

				double infgr = (inf-ent_t)/iv;
				if(infgr>=max_ent) {
					max_ent = infgr;
					attr_i = i;
				}
			}
		}
	}
	
	int choose(const Sample<T>& sam) const {
		typename map<T,int>::const_iterator it = attr_v.find(sam[attr_i]);
		if(it != attr_v.end())
			return it->second;
		else
			return 0;		
	}
public:
	virtual void initialTrain(const vector< Sample<T> >& samples, unsigned int c) {
		vector< map<T, Quantity> > q;
		this->children.push_back(new DTResultNode<T>(U));

		computeEntrophy(samples, c, q);

		int id=1;//0 zajete przez awaryjny
		for(typename map<T,Quantity>::const_iterator it=q[attr_i].begin();it!=q[attr_i].end();++it) {
			attr_v[it->first] = id++;

			vector< Sample<T> > n_samples;
			for (typename vector< Sample<T> >::const_iterator s_it=samples.begin();s_it!=samples.end();++s_it) {
				if (s_it->at(attr_i) == it->first) {
					n_samples.push_back(*s_it);
				}
			}

			if (!n_samples.empty()) {
				if (it->second.intersection.size() == 1) {
					this->children.push_back(new DTResultNode<T>(it->second.intersection.begin()->first));
				}
				else {
					NominalTestNode* ctn = new NominalTestNode();
					ctn->initialTrain(n_samples, c);
					this->children.push_back(ctn);
				}
			}
		}
	}
	
	virtual void clearBranch() {
		DTTestNode<T>::clearBranch();
		attr_v.clear();
	}
};

#endif	/* NOMINALTESTNODE_HPP */

