/* 
 * Pawel Kubik
 * Klasy sytuacji wyjatkowych
 */

#include <stdexcept>

using namespace std;

#ifndef ERROR_H
#define	ERROR_H

class EmptyArgument : virtual public invalid_argument {
public:
	EmptyArgument(const string& funName) : invalid_argument(funName) {}
};

#endif	/* ERROR_H */

