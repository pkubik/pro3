/* 
 * Pawel Kubik
 * Implementacja konkretnego drzewa
 */

#include <string>
#include "DTree.hpp"
#include "NominalTestNode.hpp"
#include "CommandProcessor.h"

using namespace std;

#ifndef TESTER_H
#define	TESTER_H

class Tester {
private:
	enum ValidationState {NO, YES, NO_CHECK};
	
	DTree<char, NominalTestNode<char,'?'> > dt;
	vector< Sample<char> > train_samples;
	vector< Sample<char> > test_samples;
	bool quit_flag;
	mutable ValidationState train_validation_flag;
	mutable ValidationState test_validation_flag;
	CommandProcessor cp;
public:
	Tester();
	bool quitFlag() const { return quit_flag; }
	
	void loadSamples(vector< Sample<char> >& samples, ValidationState& v_flag);
	
	void initTree();
	
	void addSample(vector< Sample<char> >& samples, ValidationState& v_flag);
	
	bool validateTestSample(const Sample<char>& sam) const;
	
	bool validateTestSamples() const;
	bool validateTrainSamples() const;
	bool validateSamples(const vector< Sample<char> >& samples, ValidationState& v_flag, unsigned int size) const;
	void printTrainValidation() const;
	void printTestValidation() const;
	
	void testSample() const;
	void groupTest() const;
	void passControl();
	
	void clearTree();
	void clearTrain();
	void clearTest();
	
	void printHelp() const;
	
	void quit() { quit_flag = true; }
	
	static void unrecognisedCommand();
};

istream& operator >>(istream& is, Sample<char>& sam);

#endif	/* TESTER_H */

