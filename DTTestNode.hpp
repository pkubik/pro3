/* 
 * Pawel Kubik
 * Wierzcholek testujacy (nie lisc)
 */

#include "DTNode.hpp"
#include "DTResultNode.hpp"
#include <vector>
#include <stdexcept>

using namespace std;

#ifndef DTTESTNODE_HPP
#define	DTTESTNODE_HPP

template <class T>
class DTTestNode : public DTNode<T> {
protected:
	vector< DTNode<T>* > children;
	virtual int choose(const Sample<T>& sam) const = 0;
public:
	virtual void initialTrain(const vector< Sample<char> >& samples, unsigned int c) = 0;
	
	virtual const T& getResult(const Sample<T>& sam) const {
		return children[choose(sam)]->getResult(sam);
	}
	
	virtual void clearBranch() {
		for(unsigned int i=0;i<children.size();i++)
			delete children[i];
		children.clear();
	}
	
	virtual ~DTTestNode() {
		for(unsigned int i=0;i<children.size();i++)
			delete children[i];
	}
};

class InvalidSampleSize : virtual public invalid_argument {
private:
	int correct_size;
public:
	InvalidSampleSize(int correct) : invalid_argument("invalid sample size"), correct_size(correct) {}
	int getCorrectSize() const { return correct_size; }
};

#endif	/* DTTESTNODE_HPP */

